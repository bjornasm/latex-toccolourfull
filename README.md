Latex Table Of Contents Generator
=================================

Generate table of contents from latex master files.

Usage
-----

    python toc.py my-file.tex


