#!/usr/bin/env python

import re
from collections import OrderedDict
import argparse

p = argparse.ArgumentParser()
p.add_argument("master_file")
args = p.parse_args()

main = args.master_file

input_sd = {
    'regex': re.compile("\\input{(.*)}"),
    'format': "\\input{{{}}}"
}

include_sd = {
    'regex': re.compile("\\include{(.*)}"),
    'format': "\\include{{{}}}",
}

recursions = [input_sd, include_sd]


def parse(filepath):
    try:
        with open(filepath, 'r') as file:
            contents = "".join(file.readlines())
            for r in recursions:
                for file in r['regex'].findall(contents):
                    filepath = file + ".tex"
                    str = parse(filepath)
                    search  = r['format'].format(file)
                    contents = contents.replace(search, str)
        return contents
    except IOError as e:
        return ""


all_tex = parse(main)


def get_with_lines(sec):
    r = re.compile(r"\\" + sec + r"{(.*?)}")
    out = {}
    for match in r.finditer(all_tex):
        out[match.span()[0]] = match.groups()[0]
    return OrderedDict(sorted(out.items()))


chapters = get_with_lines('chapter')
sections = get_with_lines('section')
subsections = get_with_lines('subsection')


def get_dict_index(index, dict):
    last = None
    for i, key in enumerate(dict):
        if key <= index:
            last = key
    return last

class _bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKRED = '\033[31m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def _color_surround(val, color):
    return color + str(val) + _bcolors.ENDC

def blue(name):
    return _color_surround(name, _bcolors.OKBLUE)

def red(name):
    return _color_surround(name, _bcolors.OKRED)

def green(name):
    return _color_surround(name, _bcolors.OKGREEN)

def err(name):
    return _color_surround(name, _bcolors.OKRED + _bcolors.FAIL)

class ChapterSection:

    def __init__(self, index, type):
        self.index = index
        self.section_index = None
        self.subsection_index = None

        if type == 'chapter':
            self.chapter_index = index
        elif type == 'section':
            self.chapter_index = get_dict_index(index, chapters)
            self.section_index = index
        elif type == 'subsection':
            self.chapter_index = get_dict_index(index, chapters)
            self.section_index = get_dict_index(index, sections)
            self.subsection_index = index

        self.chapter = chapters[self.chapter_index]
        if self.section_index:
            self.section = sections[self.section_index]
        if self.subsection_index:
            self.subsection = subsections[self.subsection_index]

        self.highlights = []

    def add_highlight(self, hl):
        self.highlights.append(hl)

    def __repr__(self):
        if self.section_index:
            return str(self.chapter) + "->" + str(self.section)
        return str(self.chapter)

    def __str__(self):
        name = self.chapter
        margin = ''
        out = []
        if self.section_index:
            margin = 4*" "
            name = "- " + (self.section)
        else: # Chapter
            out.append("\n")
        if self.subsection_index:
            margin = 8*" "
            name = "- " + self.subsection

        out.append(margin + name)

        if self.highlights:
            for hl in self.highlights:
                for line in hl:
                    out.append(margin +" "*2 +  red("| ") +line)

        return "\n".join(out)

def gen_chapsecs():
    chapsecs = []
    chapsecs.extend([ChapterSection(x, 'chapter') for x in chapters])
    chapsecs.extend([ChapterSection(x, 'section') for x in sections])
    chapsecs.extend([ChapterSection(x, 'subsection') for x in subsections])
    chapsecs = sorted(chapsecs, key=lambda x: x.index)
    return chapsecs

def get_nearest_chapsec(index, chapsecs):
    last_i = None
    for i, cs in enumerate(chapsecs):
        if cs.index < index:
            last_i = i
    if last_i:
        return chapsecs[last_i]

def strip_empty_lines_before_after(str):
    lines = str.split("\n")
    lines = [x.replace("\t", 4*" ") for x in lines if x.strip() != ""]
    first_len = len(lines[0]) - len(lines[0].lstrip())
    lines = [x[first_len:] for x in lines]
    return lines

def work(chapsecs):
    highlight_begin_rx = re.compile(r"\\begin{highlight}")
    highlight_end_rx = re.compile(r"\\end{highlight}")

    m_begin = highlight_begin_rx.finditer(all_tex)
    m_end = highlight_end_rx.finditer(all_tex)
    for begin, end in zip(m_begin, m_end):
        highlight_txt = []
        start_index = begin.span()[1]
        end_index = end.span()[0]

        chapsec = get_nearest_chapsec(start_index, chapsecs)
        matched_text = all_tex[start_index:end_index]
        matched_text = strip_empty_lines_before_after(matched_text)

        chapsec.add_highlight(matched_text)

    for t in ["todo", "hl"]:
        prog = re.compile(r"\\" + t + r"{(.*)}")
        for m in prog.finditer(all_tex):
            chapsec = get_nearest_chapsec(m.span()[0], chapsecs)
            if chapsec:
                chapsec.add_highlight([blue(t) + " " +m.groups()[0]])


chapsecs = gen_chapsecs()
work(chapsecs)

for cs in chapsecs:
    print(cs)
